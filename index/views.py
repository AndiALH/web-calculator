from django.shortcuts import render
from django.http import HttpResponseRedirect
import requests
from rest_framework import status


# Create your views here.
def index(request):
    return render(request, 'index.html')

def calculate(request):
    if request.method == "POST":
        num1 = request.POST['num1']
        num2 = request.POST['num2']

        # test using api.mathjs
        # url = 'http://api.mathjs.org/v4/?expr=' + num1 + '%2B' + num2
        # response = requests.get(url)
        # answer = response.content.decode("utf-8")
        # return HttpResponseRedirect('')

        data = {
            'num1' : num1,
            'num2' : num2,
        }
        # url = 'http://127.0.0.1:8000/addition/'
        url = 'https://restapi-cal.herokuapp.com/addition/'
        response = requests.get(url, data=data)
        if response.content.decode("utf-8") == "{}":
            answer = "Error : Please fill the number on all fields"
        else:
            answer = response.content.decode("utf-8")

    else:
        answer = 0
    return render(request, 'result.html', {"answer":answer})
